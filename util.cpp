#include "util.h"
#include <string>
#include <iostream>
#include <vector>

//using namespace 'std';

#include <iostream>
#include <vector>
 
 /*
int main()
{
    // Create a vector containing integers
 
    // Add two more integers to vector
    v.push_back(25);
    v.push_back(13);
 
    // Print out the vector
    std::cout << "v = { ";
    for (int n : v) {
        std::cout << n << ", ";
    }
    std::cout << "}; \n";
}
*/


class Foo {

    std::string alpha = "default";
    //int i = 1;

public:

    Foo(std::string value) {
        alpha = value;
    }

    void show() {
       std::cout << alpha << std::endl;
    }

};

class Bar {

    std::vector<std::shared_ptr<Foo> > alist = {};

public:

    Bar() {
        //alist = {}
    }

    void add(std::shared_ptr<Foo> p) {
        alist.push_back(p);
    }

    void show() {
        for (std::shared_ptr<Foo> p : alist) {
            p->show();
        }
    }
};


int test_new_cpp(void) {

    std::cout << "---" << std::endl;

    auto p = std::make_shared<Foo>("blarg");
    auto p2 = std::make_shared<Foo>("blarg2");


    p->show();
    p2->show();

    std::cout << "---" << std::endl;


    //auto b2 = new Bar(2);
    auto b2 = std::unique_ptr<Bar>(new Bar());
    b2->add(p);
    b2->add(p2);
    b2->show();
    //delete(b2);

    std::cout << "---" << std::endl;

    return 1;

}