warp5
=====

Warp5 is a new incarnation of laserllama's MIDI sequencer project.
The fifth time is the charm.

Docs
====

once I figure out what I'm doing, maybe

Building on OS X
================

OS X is required.

Warp5 is written in C++ and uses RTMidi for MIDI support and imgui
for the user interface. Modern C++ language features are used so
not all compilers or default flags will work.

Technically both of these should be fairly portable, with changes
to the imgui wrapper code and Makefiles, but I'm not really going
to work on maintaining any of that for the near future.

* install xcode and homebrew
* clone rtmidi from github
* clone imgui from github 
* brew install sdl glfw
* edit the makefile to point to rtmidi and imgui clone paths
* run make

Building elsewhere
==================

* maybe later

License
=======

BSD3 with attribution (see LICENSE.txt)

(C) Michael DeHaan, 2021. 



