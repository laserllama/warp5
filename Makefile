#
# You will need GLFW (http://www.glfw.org):
#   brew install glfw
#

CXX = g++ -std=c++17
#CXX = clang++

EXE = warp5

# FIXME: you'll have to change this if building on your own computer
IMGUI_DIR = /Users/michaeldehaan/Devel/imgui/
RTMIDI_DIR = /Users/michaeldehaan/Devel/rtmidi/
SOURCES = $(IMGUI_DIR)/imgui.cpp $(IMGUI_DIR)/imgui_demo.cpp $(IMGUI_DIR)/imgui_draw.cpp $(IMGUI_DIR)/imgui_tables.cpp $(IMGUI_DIR)/imgui_widgets.cpp
SOURCES += $(RTMIDI_DIR)/RtMidi.cpp
SOURCES += main.mm
# BOOKMARK add all our CPP files here
SOURCES += midi_demo.cpp util.cpp
SOURCES += $(IMGUI_DIR)/backends/imgui_impl_glfw.cpp $(IMGUI_DIR)/backends/imgui_impl_metal.mm
OBJS = $(addsuffix .o, $(basename $(notdir $(SOURCES))))

LIBS = -framework Metal -framework MetalKit -framework Cocoa -framework IOKit -framework CoreVideo -framework CoreMIDI -framework CoreFoundation -framework QuartzCore -framework CoreServices -framework AudioUnit -framework AudioToolbox -framework CoreAudio
LIBS += -L/usr/local/lib -L/opt/homebrew/lib
LIBS += -lglfw

CXXFLAGS = -I$(IMGUI_DIR) -I$(IMGUI_DIR)/backends -I$(RTMIDI_DIR) -I/usr/local/include -I/opt/homebrew/include
CXXFLAGS += -Wall -Wformat
CFLAGS = $(CXXFLAGS)

%.o:%.cpp
	$(CXX) $(CXXFLAGS) -D__MACOSX_CORE__ -c -o $@ $<

%.o:$(IMGUI_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o:$(RTMIDI_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -D__MACOSX_CORE__ -c -o $@ $< 

%.o:$(IMGUI_DIR)/backends/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o:%.mm
	$(CXX) $(CXXFLAGS) -ObjC++ -fobjc-weak -fobjc-arc -c -o $@ $<

%.o:$(IMGUI_DIR)/backends/%.mm
	$(CXX) $(CXXFLAGS) -ObjC++ -fobjc-weak -fobjc-arc -c -o $@ $<

all: $(EXE)
	@echo Build complete

$(EXE): $(OBJS)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LIBS)

clean:
	rm -f $(EXE) $(OBJS)
